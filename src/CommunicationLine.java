/**
 * Represents Edge
 * 
 * @author Rasmus
 * @version 1.2
 */
public class CommunicationLine implements Comparable<CommunicationLine> {

	private SwitchingCenter mC1;
	private SwitchingCenter mC2;
	private int mBandwidth;
	private int mUsedBandwidth;

	public CommunicationLine(int bandwidth, SwitchingCenter c1,
			SwitchingCenter c2) {
		mBandwidth = bandwidth;
		mC1 = c1;
		mC2 = c2;
	}

	/**
	 * @return First endpoint
	 */
	public SwitchingCenter getFirstCenter() {
		return mC1;
	}

	/**
	 * @return Second endpoint
	 */
	public SwitchingCenter getSecondCenter() {
		return mC2;
	}

	/**
	 * @return Line bandwitdh
	 */
	public int getBandwidth() {
		return mBandwidth;
	}

	/**
	 * @return Used bandwidth
	 */
	public int getUsedBandwidth() {
		return mUsedBandwidth;
	}

	/**
	 * @return Free bandwidth
	 */
	public int getRemainingBandwidth() {
		return mBandwidth - mUsedBandwidth;
	}

	/**
	 * Reserver line bandwidth
	 * 
	 * @param amount
	 *            amount to reserve
	 */
	public void reserveBandwidth(int amount) {
		mUsedBandwidth += amount;

		if (mUsedBandwidth > mBandwidth) {
			throw new RuntimeException(String.format(
					"%s runs bandwidth over the limit!", this));
		}
	}

	/**
	 * @return If there is free bandwidth left
	 */
	public boolean isMaxedOut() {
		return mUsedBandwidth == mBandwidth;
	}

	@Override
	public String toString() {
		return String.format("Connects %s to %s [%d Gbps]",
				mC1.getCenterCode(), mC2.getCenterCode(), mBandwidth);
	}

	@Override
	public int compareTo(CommunicationLine other) {
		return Integer.compare(mBandwidth, other.mBandwidth);
	}
}