import java.util.ArrayList;
import java.util.List;

/**
 * Provides algorithm to find paths between switching centers
 * 
 * @author Rasmus
 * @version 1.2
 */
public class Scanner {

	private SwitchingCenter mPrevious;
	private SwitchingCenter mTo;

	private List<SwitchingCenter> mVisited;

	protected Scanner(SwitchingCenter a, SwitchingCenter b) {
		mTo = b;
		mVisited = new ArrayList<SwitchingCenter>();
	}

	/**
	 * Reset scanner to initial state
	 */
	public void Reset() {
		mVisited.clear();
		mPrevious = null;
	}

	/**
	 * 
	 * @param a
	 *            Source switching centre
	 * @param b
	 *            Destination switching centre
	 * @return Maximum flow between switching centres
	 */
	public static int CalculateMaxFlow(SwitchingCenter a, SwitchingCenter b) {

		if (a.equals(b)) {
			return -1;
		}

		Scanner scanner = new Scanner(a, b);
		int flow = 0;

		List<CommunicationLine> path;
		while ((path = scanner.Scan(a, new ArrayList<CommunicationLine>())) != null) {
			int currentFlow = CalculateFlow(path);
			flow += currentFlow;

			Update(path, currentFlow);
			scanner.Reset();
		}

		return flow;
	}

	private static void Update(List<CommunicationLine> path, int flow) {
		for (CommunicationLine line : path) {
			line.reserveBandwidth(flow);
		}
	}

	private static int CalculateFlow(List<CommunicationLine> path) {
		int minimal = Integer.MAX_VALUE;
		for (CommunicationLine line : path) {
			int remaining = line.getRemainingBandwidth();
			if (remaining < minimal) {
				minimal = remaining;
			}
		}

		return minimal;
	}

	private List<CommunicationLine> Scan(SwitchingCenter from,
			List<CommunicationLine> currentPath) {

		// No point to go back
		if (from == mPrevious)
			return null;

		// Already there
		if (from == mTo)
			return null;

		// Look if we already been there
		if (!mVisited.contains(from)) {
			mVisited.add(from);

			for (CommunicationLine line : from.getLines()) {
				
				// line does not have free bandwidth
				// no point to go further
				if (line.isMaxedOut()) {
					continue;
				}

				currentPath.add(line);

				if (line.getFirstCenter() == mTo
						|| line.getSecondCenter() == mTo) {
					return currentPath;
				}

				mPrevious = from;
				List<CommunicationLine> result = Scan(line.getFirstCenter(),
						currentPath);
				if (result != null) {
					return result;
				}

				result = Scan(line.getSecondCenter(), currentPath);
				if (result != null) {
					return result;
				}

				currentPath.remove(line);
			}
		}

		return null;
	}
}