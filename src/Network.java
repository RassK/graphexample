import java.util.ArrayList;
import java.util.List;

/**
 * Represents Graph with full duplex Edges
 * 
 * @author Rasmus
 * @version 1.1
 */
public class Network {

	private List<SwitchingCenter> mCenters;

	public Network() {
		mCenters = new ArrayList<SwitchingCenter>();
	}

	/**
	 * Add switching centre to Network graph
	 * 
	 * @param center
	 *            Switching centre to add
	 */
	public void addCenter(SwitchingCenter center) {
		mCenters.add(center);
	}

	/**
	 * Get all switching centres from Network
	 * 
	 * @return List of switching centres
	 */
	public List<SwitchingCenter> getCenters() {
		return mCenters;
	}
}