import java.util.ArrayList;
import java.util.List;

/**
 * @author Rasmus
 * @version 1.1
 */
public class SwitchingCenter {

	private String mCode;
	private List<CommunicationLine> mLines;

	public SwitchingCenter(String code) {
		mCode = code;
		mLines = new ArrayList<CommunicationLine>();
	}

	/**
	 * Adds a connection related with center
	 * 
	 * @param line
	 *            Connection line between 2 switching centers
	 * @throws Exception
	 *             Connection line is not related to centre
	 */
	public void addLine(CommunicationLine line) throws Exception {

		if (line.getFirstCenter() != this && line.getSecondCenter() != this) {
			throw new Exception(
					String.format(
							"The connection line is not related with this (%s) centre. Line: %s",
							this, line));
		}

		mLines.add(line);
	}

	/**
	 * Get all lines related to this center
	 * 
	 * @return Center's connections
	 */
	public List<CommunicationLine> getLines() {
		return mLines;
	}

	/**
	 * Center's identification code
	 * 
	 * @return Identification code
	 */
	public String getCenterCode() {
		return mCode;
	}

	@Override
	public String toString() {
		return "Switching center " + mCode;
	}
}