/**
 * @author Rasmus
 * @version 1.2
 *
 */
public class Program {

	public static void main(String[] args) {

		// Build a small graph per time and connect pieces to larger one
		// Builds 6 * 1001 graph ( 6006 connected elements )
		Network main = buildNetwork(1);
		for (int i = 0; i < 1000; i++) {
			Network n = buildNetwork(i + 2);
			main = connectNetwork(main, n);
		}

		// SwitchingCenter a = main.getCenters().get(0);
		// SwitchingCenter b = main.getCenters().get(main.getCenters().size() -
		// 1);
		SwitchingCenter a = main.getCenters().get(3);
		SwitchingCenter b = main.getCenters().get(125);

		System.out.println(String
				.format("Looking for path from %s to %s", a, b));
		if (a == b) {
			System.out.println("Its the same switching center!");
			return;
		}

		int flow = Scanner.CalculateMaxFlow(a, b);
		System.out.println("Maximum flow between the switching centers is "
				+ flow);
	}

	/**
	 * Connect 2 smaller graphs together
	 * 
	 * @param a
	 *            Graph to connect
	 * @param b
	 *            Graph other Graph
	 * @return Graph a where b is connected to
	 */
	public static Network connectNetwork(Network a, Network b) {

		SwitchingCenter c1 = b.getCenters().get(0);
		SwitchingCenter c2 = a.getCenters().get(a.getCenters().size() - 1);

		CommunicationLine line = new CommunicationLine(10, c1, c2);

		try {
			c1.addLine(line);
			c2.addLine(line);
		} catch (Exception e) {

		}

		for (SwitchingCenter center : b.getCenters()) {
			a.addCenter(center);
		}

		return a;
	}

	/**
	 * Builds a sample Network Graph
	 * 
	 * @return Network Graph
	 */
	public static Network buildNetwork(int nr) {
		SwitchingCenter c1 = new SwitchingCenter("Alpha " + nr);
		SwitchingCenter c2 = new SwitchingCenter("Beta " + nr);
		SwitchingCenter c3 = new SwitchingCenter("Gamma " + nr);
		SwitchingCenter c4 = new SwitchingCenter("Delta " + nr);
		SwitchingCenter c5 = new SwitchingCenter("Epsilon " + nr);
		SwitchingCenter c6 = new SwitchingCenter("Zeta " + nr);

		CommunicationLine e1 = new CommunicationLine(10, c1, c2);
		CommunicationLine e2 = new CommunicationLine(20, c1, c5);
		CommunicationLine e3 = new CommunicationLine(30, c2, c5);
		CommunicationLine e4 = new CommunicationLine(15, c2, c3);
		CommunicationLine e5 = new CommunicationLine(20, c3, c4);
		CommunicationLine e6 = new CommunicationLine(5, c4, c5);
		CommunicationLine e7 = new CommunicationLine(10, c4, c6);

		/* Connect */
		try {
			c1.addLine(e1);
			c1.addLine(e2);

			c2.addLine(e1);
			c2.addLine(e3);
			c2.addLine(e4);

			c3.addLine(e4);
			c3.addLine(e5);

			c4.addLine(e5);
			c4.addLine(e6);
			c4.addLine(e7);

			c5.addLine(e3);
			c5.addLine(e2);
			c5.addLine(e6);

			c6.addLine(e7);
		} catch (Exception e) {
			System.out
					.println("Error occurred while generating Network graph: "
							+ e.getMessage());
			return null;
		}

		Network n = new Network();
		n.addCenter(c1);
		n.addCenter(c2);
		n.addCenter(c3);
		n.addCenter(c4);
		n.addCenter(c5);
		n.addCenter(c6);

		return n;
	}
}