import static org.junit.Assert.*;

import java.util.List;
import org.junit.Test;

public class NetworkTest {

	@Test(timeout = 20000)
	public void validNetwork() {
		List<SwitchingCenter> verteces = GetStandardNetwork().getCenters();
		assertTrue(verteces.size() == 60);
	}

	@Test(timeout = 20000, expected = RuntimeException.class)
	public void linesCannotBeOverfilled() {
		SwitchingCenter a = new SwitchingCenter("Alpha");
		SwitchingCenter b = new SwitchingCenter("Beta");
		CommunicationLine line = new CommunicationLine(15, a, b);

		line.reserveBandwidth(20);
	}

	@Test(timeout = 20000)
	public void linesAreGettingFull() {
		SwitchingCenter a = new SwitchingCenter("Alpha");
		SwitchingCenter b = new SwitchingCenter("Beta");
		CommunicationLine line = new CommunicationLine(15, a, b);

		line.reserveBandwidth(15);
		assertTrue(line.isMaxedOut());
	}

	@Test(timeout = 20000)
	public void identifySameSourceAndDestination() {
		Network network = GetStandardNetwork();
		SwitchingCenter a = network.getCenters().get(15);
		SwitchingCenter b = network.getCenters().get(15);

		int maxFlow = Scanner.CalculateMaxFlow(a, b);
		assertTrue(maxFlow == -1);
	}

	@Test(timeout = 2000)
	public void hasCircularPath() {
		List<SwitchingCenter> centers = GetStandardNetwork().getCenters();

		SwitchingCenter start = centers.get(1);

		SwitchingCenter beta = start // Starting point is Gamma
				.getLines().get(1) // Get line Beta => Epsilon
				.getSecondCenter() // Get Epsilon
				.getLines().get(2) // Get line Epsilon => Delta
				.getFirstCenter() // Get Delta
				.getLines().get(0) // Get line Delta => Gamma
				.getFirstCenter() // Get Gamma
				.getLines().get(0) // Get line Gamma => Beta
				.getFirstCenter(); // Get Gamma

		// Must be the same object
		assertSame(start, beta);
	}

	@Test(timeout = 20000)
	public void highestBandwidthIsRight1() {

		Network network = GetStandardNetwork();
		SwitchingCenter a = network.getCenters().get(0);
		SwitchingCenter b = network.getCenters().get(16);

		int maxFlow = Scanner.CalculateMaxFlow(a, b);
		assertEquals(maxFlow, 10);
	}
	
	@Test(timeout = 20000)
	public void highestBandwidthIsRight2() {

		Network network = GetStandardNetwork();
		SwitchingCenter a = network.getCenters().get(3);
		SwitchingCenter b = network.getCenters().get(4);

		int maxFlow = Scanner.CalculateMaxFlow(a, b);
		assertEquals(maxFlow, 20);
	}
	
	@Test(timeout = 20000)
	public void highestBandwidthIsRight3() {

		Network network = GetStandardNetwork();
		SwitchingCenter a = network.getCenters().get(4);
		SwitchingCenter b = network.getCenters().get(0);

		int maxFlow = Scanner.CalculateMaxFlow(a, b);
		assertEquals(maxFlow, 30);
	}


	private Network GetStandardNetwork() {
		Network main = Program.buildNetwork(1);
		for (int i = 0; i < 9; i++) {
			Network n = Program.buildNetwork(i + 2);
			main = Program.connectNetwork(main, n);
		}

		return main;
	}
}
