Task
-----------
Consider a diagram of a telephone network, which is a graph G whose vertices represent switching centers, and whose edges represent communication lines joining pairs of centers. 
Edges are marked by their bandwidth, and the bandwidth of a path is the bandwidth of its lowest bandwidth edge. 
Give an algorithm that, given a diagram and two switching centers a and b, output the maximum bandwitdh of a path between a and b.

Solution
-----------------
Simple solution that backtracks through all of the possible ways from point A to point B and maps all of the right paths for later analysis.